//
//  LoginResult.swift
//  Shop
//
//  Created by Margarita Novokhatskaia on 16/01/2022.
//

import Foundation

struct LoginResult: Codable {
    let result: Int
    let user: User
}
